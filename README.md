# urdna2015.c

Implementation of Universal RDF Dataset Canonicalization Algorithm 2015 ([URDNA2015][]) in C.

## Status

59/62 tests passing. Some memory leaks.

[URDNA2015]: https://w3c-ccg.github.io/rdf-dataset-canonicalization/spec/#dfn-urdna2015
