# libutil

- Upstream: https://git.suckless.org/sbase/file/libutil/sha256.c.html
- Upstream: https://git.suckless.org/sbase/file/sha256.h.html
- SPDX-FileCopyrightText: 2012-2014 Luke Dashjr
 Public domain, by Ulrich Drepper et al.
- SPDX-License-Identifier: Public Domain
