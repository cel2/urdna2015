// SPDX-FileCopyrightText: 2022 Charles E. Lehner
// SPDX-License-Identifier: AGPL-3.0-or-later
#include <ctype.h>
#include <err.h>
#include <errno.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "libutil/sha256.h"

int allocs;
int frees;

#define malloc(x) (allocs++, malloc(x))
#define strdup(x) (allocs++, strdup(x))
#define calloc(a, b) (allocs++, calloc(a, b))
#define getline(a, b, c) (allocs++, getline(a, b, c))
#define free(x) (x ? frees++ : (frees += 10000), free(x))

struct hash {
	unsigned char data[32];
};

struct blank_node_identifier {
	char *str;
	size_t len;
};

struct iri_ref {
	const char *str;
	size_t len;
};

struct blank_node_label {
	const char *str;
	size_t len;
};

struct literal {
	const char *str;
	size_t len;
};

enum rdf_term_type {
	rdf_term_type_null,
	rdf_term_type_iri_ref,
	rdf_term_type_blank_node_label,
	rdf_term_type_literal,
};

struct rdf_subject {
	enum rdf_term_type type;
	union {
		struct iri_ref iri_ref;
		struct blank_node_label blank_node_label;
	} value;
};

struct rdf_predicate {
	struct iri_ref iri_ref;
};

struct rdf_object {
	enum rdf_term_type type;
	union {
		struct iri_ref iri_ref;
		struct blank_node_label blank_node_label;
		struct literal literal;
	} value;
};

struct rdf_graph_label_opt {
	enum rdf_term_type type;
	union {
		struct iri_ref iri_ref;
		struct blank_node_label blank_node_label;
	} value;
};

struct rdf_statement {
	char *line;
	struct rdf_subject subject;
	struct rdf_predicate predicate;
	struct rdf_object object;
	struct rdf_graph_label_opt graph_label_opt;
	bool modified;
};

struct rdf_statement_list {
	struct rdf_statement_list *prev;
	struct rdf_statement stmt;
};

struct iri_ref_or_blank_node_label {
	enum rdf_term_type type;
	union {
		struct iri_ref iri_ref;
		struct blank_node_label blank_node_label;
	} value;
};

struct iri_ref_or_blank_node_label_or_literal {
	enum rdf_term_type type;
	union {
		struct iri_ref iri_ref;
		struct blank_node_label blank_node_label;
		struct literal literal;
	} value;
};

// https://json-ld.github.io/rdf-dataset-canonicalization/spec/#dfn-issued-identifiers-list
struct issued_identifiers_list_item {
	struct issued_identifiers_list_item *prev;
	struct blank_node_label existing_identifier;
	struct blank_node_identifier issued_identifier;
};

// https://json-ld.github.io/rdf-dataset-canonicalization/spec/#blank-node-identifier-issuer-state
struct blank_node_identifier_issuer_state {
	char identifier_prefix[32];
	unsigned int identifier_counter;
	struct issued_identifiers_list_item *issued_identifiers_list;
};

struct quads_refs {
	struct quads_refs *prev;
	struct rdf_statement *quad;
};

// https://json-ld.github.io/rdf-dataset-canonicalization/spec/#dfn-blank-node-to-quads-map
struct blank_node_to_quads_map {
	struct blank_node_to_quads_map *prev;
	struct blank_node_label bni;
	struct quads_refs *quads;
};

struct blank_node_identifier_list {
	struct blank_node_identifier_list *prev;
	struct blank_node_label bni;
};

// https://json-ld.github.io/rdf-dataset-canonicalization/spec/#dfn-hash-to-blank-nodes-map
struct hash_to_blank_nodes_map {
	struct hash_to_blank_nodes_map *prev;
	struct hash hash;
	struct blank_node_identifier_list *list;
};

// https://json-ld.github.io/rdf-dataset-canonicalization/spec/#canonicalization-state
struct canonicalization_state {
	struct blank_node_to_quads_map *blank_node_to_quads_map;
	struct hash_to_blank_nodes_map *hash_to_blank_nodes_map;
	struct blank_node_identifier_issuer_state canonical_issuer;
};

struct hash_path_list_item {
	struct blank_node_identifier_issuer_state *issuer;
	struct hash hash;
};

void identifier_issuer_init(struct blank_node_identifier_issuer_state *identifier_issuer, const char *prefix) {
	identifier_issuer->identifier_counter = 0;
	identifier_issuer->issued_identifiers_list = NULL;
	strncpy(identifier_issuer->identifier_prefix, prefix, sizeof identifier_issuer->identifier_prefix);
}

void canonicalization_state_init(struct canonicalization_state *state) {
	state->blank_node_to_quads_map = NULL;
	state->hash_to_blank_nodes_map = NULL;
	identifier_issuer_init(&state->canonical_issuer, "_:c14n");
}

void quads_refs_free(struct quads_refs *quads) {
	while (quads != NULL) {
		struct quads_refs *prev = quads->prev;
		// quad is assumed to not need to be freed
		free(quads);
		quads = prev;
	}
}

void blank_node_to_quads_map_free(struct blank_node_to_quads_map *map) {
	while (map != NULL) {
		struct blank_node_to_quads_map *prev = map->prev;
		// bni is assumed to not need to be freed
		quads_refs_free(map->quads);
		free(map);
		map = prev;
	}
}

void blank_node_identifier_list_free(struct blank_node_identifier_list *list) {
	while (list != NULL) {
		struct blank_node_identifier_list *prev = list->prev;
		// bni is assumed to not need to be freed
		free(list);
		list = prev;
	}
}

void hash_to_blank_nodes_map_free(struct hash_to_blank_nodes_map *map) {
	while (map != NULL) {
		struct hash_to_blank_nodes_map *prev = map->prev;
		// hash is assumed to not need to be freed
		blank_node_identifier_list_free(map->list);
		free(map);
		map = prev;
	}
}

void issued_identifiers_list_free(struct issued_identifiers_list_item *li) {
	struct issued_identifiers_list_item *i, *prev;
	for (i = li; i != NULL; i = prev) {
		free(i->issued_identifier.str);
		// Note: existing identifier not freed.
		prev = i->prev;
		free(i);
	}
}

void issuer_state_deinit(struct blank_node_identifier_issuer_state *issuer) {
	issued_identifiers_list_free(issuer->issued_identifiers_list);
}

void issuer_state_free(struct blank_node_identifier_issuer_state *issuer) {
	issuer_state_deinit(issuer);
	free(issuer);
}

void canonicalization_state_deinit(struct canonicalization_state *state) {
	blank_node_to_quads_map_free(state->blank_node_to_quads_map);
	hash_to_blank_nodes_map_free(state->hash_to_blank_nodes_map);
	state->blank_node_to_quads_map = NULL;
	state->hash_to_blank_nodes_map = NULL;
	issuer_state_deinit(&state->canonical_issuer);
}

int parse_hexchar(char c) {
	switch (c) {
		case '0': return 0;
		case '1': return 1;
		case '2': return 2;
		case '3': return 3;
		case '4': return 4;
		case '5': return 5;
		case '6': return 6;
		case '7': return 7;
		case '8': return 8;
		case '9': return 9;
		case 'a': case 'A': return 0xa;
		case 'b': case 'B': return 0xb;
		case 'c': case 'C': return 0xc;
		case 'd': case 'D': return 0xd;
		case 'e': case 'E': return 0xe;
		case 'f': case 'F': return 0xf;
	}
	warnx("Unexpected character for hex: %c", c);
	errno = EPROTO;
	return -1;
}

char escape_slq(char c) {
	switch (c) {
		case '\n': return 'n';
		case '\r': return 'r';
		case '"': return '"';
		case '\\': return '\\';
		default: return 0;
	}
}

char unescape_slq(char c) {
	switch (c) {
		case 't': return '\t';
		case 'b': return '\b';
		case 'f': return '\f';
		case '\'': return '\'';
		default: return 0;
	}
}

int utfenc2(char **writestrp, size_t *lenp, const char buf[2], bool in_iriref) {
	char *writestr = *writestrp;
	size_t len = *lenp;
	if (buf[0] == 0) {
		// single-byte character
		// 0aaaaaaa
		if (in_iriref) {
			if (!*writestr) goto end;
			*writestr++ = buf[1];
			len++;
		} else {
			// in string literal
			char e = escape_slq(buf[1]);
			if (e) {
				if (!*writestr) goto end;
				*writestr++ = '\\';
				if (!*writestr) goto end;
				*writestr++ = e;
				len += 2;
			} else {
				if (!*writestr) goto end;
				*writestr++ = buf[1];
				len++;
			}
		}
	} else if (buf[0] < 8) {
		// double-byte character
		// 00000aaa aabbbbbb -> 110aaaaa 10bbbbbb
		if (!*writestr) goto end;
		*writestr++ = 0xc0 | (buf[0] << 4) | ((buf[1] & 0xc0) >> 6);
		if (!*writestr) goto end;
		*writestr++ = 0x80 | (buf[2] & 0x3f);
		len += 2;
	} else {
		// triple-byte character
		// aaaabbbb bbcccccc -> 1110aaaa 10bbbbbb 10cccccc
		if (!*writestr) goto end;
		*writestr++ = 0xe0 | ((buf[0] & 0xf0) >> 4);
		if (!*writestr) goto end;
		*writestr++ = 0x80 | ((buf[0] & 0xf) << 2) | (buf[1] >> 6);
		if (!*writestr) goto end;
		*writestr++ = 0x80 | ((buf[1] & 0x3f));
		len += 3;
	}
	*lenp = len;
	*writestrp = writestr;
	return 0;
end:
	errno = EMSGSIZE;
	return -1;
}

int utfenc3(char **writestrp, size_t *lenp, const char buf[3], bool in_iriref) {
	if (buf[0] == 0) return utfenc2(writestrp, lenp, buf+1, in_iriref);
	// quad-byte character
	// 000aaabb bbbbcccc ccdddddd -> 11110aaa 10bbbbbb 10cccccc 10dddddd
	char *writestr = *writestrp;
	size_t len = *lenp;
	if (!*writestr) goto end;
	*writestr++ = 0xf0 | (buf[0] & 0x1c) >> 2;
	if (!*writestr) goto end;
	*writestr++ = 0x80 | ((buf[0] & 3) << 4) | ((buf[1] & 0xf0) >> 4);
	if (!*writestr) goto end;
	*writestr++ = 0x80 | ((buf[1] & 0xf) << 2) | (buf[2] >> 6);
	if (!*writestr) goto end;
	*writestr++ = 0x80 | (buf[2] & 0x3f);
	*writestrp = writestr;
	len += 4;
	*lenp = len;
	return 0;
end:
	errno = EMSGSIZE;
	return -1;
}

int parse_uchar(char **strp, char **writestrp, size_t *lenp, bool in_iriref) {
	int rc;
	char *str = *strp;
	size_t len = *lenp;
	char *writestr = *writestrp;
	if (writestr == NULL) writestr = str;
	if (*str++ != '\\') {
		errno = EPROTO;
		return -1;
	}
	char c;
	char buf[3];
	switch (*str++) {
		case 'u':
			c = parse_hexchar(*str++); if (c < 0) return -1;
			buf[0] = c << 4;
			c = parse_hexchar(*str++); if (c < 0) return -1;
			buf[0] |= c;
			c = parse_hexchar(*str++); if (c < 0) return -1;
			buf[1] = c << 4;
			c = parse_hexchar(*str++); if (c < 0) return -1;
			buf[1] |= c;
			rc = utfenc2(&writestr, &len, buf, in_iriref);
			if (rc < 0) return -1;
			break;
		case 'U':
			c = parse_hexchar(*str++); if (c < 0) return -1;
			if (c > 0) { errno = EPROTO; return -1; }
			c = parse_hexchar(*str++); if (c < 0) return -1;
			if (c > 0) { errno = EPROTO; return -1; }
			c = parse_hexchar(*str++); if (c < 0) return -1;
			buf[0] = c << 4;
			c = parse_hexchar(*str++); if (c < 0) return -1;
			buf[0] |= c;
			c = parse_hexchar(*str++); if (c < 0) return -1;
			buf[1] = c << 4;
			c = parse_hexchar(*str++); if (c < 0) return -1;
			buf[1] |= c;
			c = parse_hexchar(*str++); if (c < 0) return -1;
			buf[2] = c << 4;
			c = parse_hexchar(*str++); if (c < 0) return -1;
			buf[2] |= c;
			rc = utfenc3(&writestr, &len, buf, in_iriref);
			if (rc < 0) return -1;
			break;
		default:
			errno = EPROTO;
			return -1;
	}
	*strp = str;
	*writestrp = writestr;
	*lenp = len;
	return 0;
}

int parse_iri_ref(struct iri_ref *iri_ref, char **strp, char **writestrp) {
	// 	IRIREF 	::= 	'<' ([^#x00-#x20<>"{}|^`\] | UCHAR)* '>'
	char *str = *strp;
	iri_ref->str = str;
	if (*str++ != '<') {
		warnx("Unexpected start of IRI ref: %c", str[-1]);
		errno = EPROTO;
		return -1;
	}
	size_t len = 1;
	char c;
	int rc;
	char *writestr = *writestrp;
	for (; (c = *str++); len++) switch(c) {
		case '\\':
			switch (*str) {
				case 'u': case 'U':
					str--;
					len--;
					rc = parse_uchar(&str, &writestr, &len, true);
					if (rc < 0) return -1;
					break;
				default:
					warnx("Unexpected escape in IRI ref: %c", str[-1]);
					errno = EPROTO;
					return -1;
			}
			break;
		case 0x00: case 0x01: case 0x02: case 0x03: case 0x04: case 0x05: case 0x06: case 0x07:
		case 0x08: case 0x09: case 0x0a: case 0x0b: case 0x0c: case 0x0d: case 0x0e: case 0x0f:
		case 0x10: case 0x11: case 0x12: case 0x13: case 0x14: case 0x15: case 0x16: case 0x17:
		case 0x18: case 0x19: case 0x1a: case 0x1b: case 0x1c: case 0x1d: case 0x1e: case 0x1f: case 0x20:
		case '<': case '"': case '{': case '}': case '|': case '^': case '`':
			warnx("Unexpected character in IRI ref: %c", c);
			errno = EPROTO;
			return -1;
		case '>':
			len++;
			if (writestr != NULL) {
				if (!*writestr) goto end;
				*writestr++ = c;
			}
			goto done;
		default:
			if (writestr != NULL) {
				if (!*writestr) goto end;
				*writestr++ = c;
			}
	}
done:
	*writestrp = writestr;
	iri_ref->len = len;
	*strp = str;
	return 0;
end:
	errno = EMSGSIZE;
	return -1;
}

int parse_blank_node_label(struct blank_node_label *blank_node_label, char **strp) {
	// BLANK_NODE_LABEL 	::= 	'_:' (PN_CHARS_U | [0-9]) ((PN_CHARS | '.')* PN_CHARS)?
	// PN_CHARS_BASE 	::= 	[A-Z] | [a-z] | [#x00C0-#x00D6] | [#x00D8-#x00F6] | [#x00F8-#x02FF] | [#x0370-#x037D] | [#x037F-#x1FFF] | [#x200C-#x200D] | [#x2070-#x218F] | [#x2C00-#x2FEF] | [#x3001-#xD7FF] | [#xF900-#xFDCF] | [#xFDF0-#xFFFD] | [#x10000-#xEFFFF]
	// PN_CHARS_U 	::= 	PN_CHARS_BASE | '_' | ':'
	// PN_CHARS 	::= 	PN_CHARS_U | '-' | [0-9] | #x00B7 | [#x0300-#x036F] | [#x203F-#x2040]
	char *str = *strp;
	blank_node_label->str = str;
	if (*str++ != '_' || *str++ != ':') {
		warnx("Unexpected prefix in blank node label: %.2s", *strp);
		errno = EPROTO;
		return -1;
	}
	char c;
	size_t len = 2;
	for (; (c = *str); str++, len++) switch(c) {
		case ' ': 
			goto done;
	}
done:
	blank_node_label->len = len;
	*strp = str;
	return 0;
}

int parse_langtag(char **strp, size_t *lenp) {
	// TODO: make const
	//  LANGTAG 	::= 	'@' [a-zA-Z]+ ('-' [a-zA-Z0-9]+)*
	char *str = *strp;
	if (*str != '@') {
		warnx("Unexpected start of langtag ref: %c", *str);
		errno = EPROTO;
		return -1;
	}
	size_t len = *lenp + 1;
	str++;
	char c;
	for (; (c = *str); str++, len++) {
		if (isalnum(c) || c == '-') {
			// TODO: stricter validation
		} else {
			break;
		}
	}
	*lenp = len;
	*strp = str;
	return 0;
}

int parse_literal(struct literal *literal, char **strp, char **writestrp) {
	// literal 	::= 	STRING_LITERAL_QUOTE ('^^' IRIREF | LANGTAG)?
	// STRING_LITERAL_QUOTE 	::= 	'"' ([^#x22#x5C#xA#xD] | ECHAR | UCHAR)* '"'
	// UCHAR 	::= 	'\u' HEX HEX HEX HEX | '\U' HEX HEX HEX HEX HEX HEX HEX HEX
	// ECHAR 	::= 	'\' [tbnrf"'\]
	char *str = *strp;
	int rc;
	literal->str = str;
	if (*str++ != '"') {
		warnx("Unexpected prefix in literal: %c", str[-1]);
		errno = EPROTO;
		return -1;
	}
	char c;
	size_t len = 1;
	char *writestr = *writestrp;
	for (; (c = *str++); len++) switch(c) {
		case '\\':
			switch (c = *str) {
				case 'u':
				case 'U':
					str--;
					len--;
					rc = parse_uchar(&str, &writestr, &len, false);
					if (rc < 0) return -1;
					break;
				case 't':
				case 'b':
				case 'f':
				case '\'':
					// unescape echar
					if (writestr == NULL) writestr = str-1;
					str++;
					if (!*writestr) goto end;
					*writestr++ = unescape_slq(c);
					break;
				case 'n':
				case 'r':
				case '"':
				case '\\':
					// echar
					str++;
					len++;
					if (writestr != NULL) {
						if (!*writestr) goto end;
						*writestr++ = '\\';
						if (!*writestr) goto end;
						*writestr++ = c;
					}
					break;
				default:
					warnx("Unexpected escape in string literal: %c", str[-1]);
					errno = EPROTO;
					return -1;
			}
			break;
		case '"':
			if (writestr != NULL) {
				if (!*writestr) goto end;
				*writestr++ = c;
			}
			len++;
			goto endquote;
		default:
			if (writestr != NULL) {
				if (!*writestr) goto end;
				*writestr++ = c;
			}
	}
	warnx("Missing end of quoted string literal");
	errno = EPROTO;
	return -1;
endquote:
	if (str[0] == '@') {
		rc = parse_langtag(&str, &len);
		if (rc < 0) return -1;
	} else if (str[0] == '^' && str[1] == '^') {
		len += 2;
		str += 2;
		struct iri_ref iri_ref;
		rc = parse_iri_ref(&iri_ref, &str, &writestr);
		if (rc < 0) return -1;
		len += iri_ref.len;
	}
	literal->len = len;
	*strp = str;
	*writestrp = writestr;
	return 0;
end:
	errno = EMSGSIZE;
	return -1;
}

int parse_iri_ref_or_blank_node_label(struct iri_ref_or_blank_node_label *iobl, char **strp, char **writestrp) {
	switch (**strp) {
		case '<':
			iobl->type = rdf_term_type_iri_ref;
			return parse_iri_ref(&iobl->value.iri_ref, strp, writestrp);
		case '_':
			iobl->type = rdf_term_type_blank_node_label;
			return parse_blank_node_label(&iobl->value.blank_node_label, strp);
		default:
			warnx("Unexpected prefix in IRI ref or blank node label: %c", **strp);
			errno = EPROTO;
			return -1;
	}
}

int rdf_subject_parse(struct rdf_subject *subj, char **strp, char **writestrp) {
	struct iri_ref_or_blank_node_label iobl;
	int rc = parse_iri_ref_or_blank_node_label(&iobl, strp, writestrp);
	if (rc < 0) return -1;
	subj->type = iobl.type;
	memcpy(&subj->value, &iobl.value, sizeof iobl.value);
	return 0;
}

int rdf_predicate_parse(struct rdf_predicate *pred, char **strp, char **writestrp) {
	return parse_iri_ref(&pred->iri_ref, strp, writestrp);
}

int parse_iri_ref_or_blank_node_label_or_literal(struct iri_ref_or_blank_node_label_or_literal *ioblol, char **strp, char **writestrp) {
	switch (**strp) {
		case '<':
			ioblol->type = rdf_term_type_iri_ref;
			return parse_iri_ref(&ioblol->value.iri_ref, strp, writestrp);
		case '_':
			ioblol->type = rdf_term_type_blank_node_label;
			return parse_blank_node_label(&ioblol->value.blank_node_label, strp);
		case '"':
			ioblol->type = rdf_term_type_literal;
			return parse_literal(&ioblol->value.literal, strp, writestrp);
		default:
			warnx("Unexpected prefix in IRI ref or blank node label or literal: %c", **strp);
			errno = EPROTO;
			return -1;
	}
}

int rdf_object_parse(struct rdf_object *obj, char **strp, char **writestrp) {
	struct iri_ref_or_blank_node_label_or_literal ioblol;
	int rc = parse_iri_ref_or_blank_node_label_or_literal(&ioblol, strp, writestrp);
	if (rc < 0) return -1;
	obj->type = ioblol.type;
	memcpy(&obj->value, &ioblol.value, sizeof ioblol.value);
	return 0;
}

int rdf_graph_label_opt_parse(struct rdf_graph_label_opt *glo, char **strp, char **writestrp) {
	if (**strp == '.') {
		glo->type = rdf_term_type_null;
		return 0;
	}
	struct iri_ref_or_blank_node_label iobl;
	int rc = parse_iri_ref_or_blank_node_label(&iobl, strp, writestrp);
	if (rc < 0) return -1;
	glo->type = iobl.type;
	memcpy(&glo->value, &iobl.value, sizeof iobl.value);
	return 0;
}

int rdf_statement_parse(struct rdf_statement *stmt) {
	char *str = stmt->line;
	bool modified = stmt->modified;
	char *writestr = NULL;
	// statement 	::= 	subject predicate object graphLabel? '.'
	int rc = rdf_subject_parse(&stmt->subject, &str, &writestr);
	if (rc < 0) return -1;
	if (*str++ != ' ') {
		warnx("Unexpected non-space after subject: %c", str[-1]);
		errno = EPROTO;
		return -1;
	}

	rc = rdf_predicate_parse(&stmt->predicate, &str, &writestr);
	if (rc < 0) return -1;
	if (*str++ != ' ') {
		warnx("Unexpected non-space after predicate: %c", str[-1]);
		errno = EPROTO;
		return -1;
	}

	rc = rdf_object_parse(&stmt->object, &str, &writestr);
	if (rc < 0) return -1;
	if (*str++ != ' ') {
		warnx("Unexpected non-space after object: %c", str[-1]);
		errno = EPROTO;
		return -1;
	}

	rc = rdf_graph_label_opt_parse(&stmt->graph_label_opt, &str, &writestr);
	if (rc < 0) return -1;
	if (stmt->graph_label_opt.type != rdf_term_type_null && *str++ != ' ') {
		warnx("Unexpected non-space after optional graph label: %c", str[-1]);
		errno = EPROTO;
		return -1;
	}

	if (*str++ != '.') {
		warnx("Unexpected end of statement line: %c", str[-1]);
		errno = EPROTO;
		return -1;
	}

	if (writestr != NULL) modified = true;
	stmt->modified = modified;
	return 0;
}

// read an nquad line. returns 0 on success, 1 on EOF, negative on error.
int readnquad(struct rdf_statement_list **listp) {
	char *line = NULL;
	size_t n;
	ssize_t sz = getline(&line, &n, stdin);
	if (sz < 0) goto noline;
	struct rdf_statement_list *list;
	list = malloc(sizeof *list);
	if (list == NULL) goto error;
	list->stmt.line = line;
	list->prev = *listp;
	int rc = rdf_statement_parse(&list->stmt);
	if (rc < 0) goto error;
	*listp = list;
	return 0;
error:
	free(list);
	free(line);
	return -1;
noline:
	free(line);
	return feof(stdin) ? 1 : -1;
}

int compare_quadsp(const void *a, const void *b) {
	char *const *quad1p = a, *quad1 = *quad1p;
	char *const *quad2p = b, *quad2 = *quad2p;
	return strcmp(quad1, quad2);
}

bool bnl_equals(const struct blank_node_label *a, const struct blank_node_label *b) {
	size_t len = a->len;
	return len == b->len && !strncmp(a->str, b->str, len);
}

void blank_node_identifier_list_remove(struct blank_node_identifier_list **listp, struct blank_node_label *identifier) {
	struct blank_node_identifier_list **next = listp, *list;
	for (list = *next; list != NULL; next = &list->prev, list = list->prev) {
		if (bnl_equals(&list->bni, identifier)) {
			*next = list->prev;
			free(list);
			return;
		}
	}
}

int add_quad_ref(struct blank_node_to_quads_map **mapp, struct blank_node_label *bnl, struct rdf_statement *quad) {
	struct blank_node_to_quads_map *map;
	struct quads_refs *quads, **quadsp = NULL;

	// look up entry in map for blank node identifier
	for (map = *mapp; map != NULL; map = map->prev) {
		if (bnl_equals(&map->bni, bnl)) {
			quadsp = &map->quads;
			break;
		}
	}
	if (quadsp == NULL) {
		// create entry if none exists
		map = malloc(sizeof *map);
		if (map == NULL) return -1;
		map->prev = *mapp;
		map->quads = NULL;
		map->bni.str = bnl->str;
	if (map->bni.str == NULL) err(1, "NO3");
		map->bni.len = bnl->len;
		*mapp = map;
		quadsp = &map->quads;
	}

	// add reference to quad
	for (quads = *quadsp; quads != NULL; quads = quads->prev) {
		// reference already exists
		if (quads->quad == quad) return 0;
	}
	quads = malloc(sizeof *quads);
	quads->prev = *quadsp;
	quads->quad = quad;
	*quadsp = quads;
	return 0;
}

int hash_compare(const struct hash *a, const struct hash *b) {
	return memcmp(&a->data, &b->data, sizeof a->data);
}

int add_hash_id(struct hash_to_blank_nodes_map **mapp, struct hash hash, struct blank_node_label *bnl) {
	struct hash_to_blank_nodes_map *map;
	struct blank_node_identifier_list *list, **listp = NULL;
	for (map = *mapp; map != NULL; map = map->prev) {
		if (hash_compare(&map->hash, &hash) == 0) {
			listp = &map->list;
			break;
		}
	}
	if (listp == NULL) {
		map = malloc(sizeof *map);
		if (map == NULL) return -1;
		map->prev = *mapp;
		map->list = NULL;
		memcpy(&map->hash, &hash, sizeof hash);
		listp = &map->list;
		*mapp = map;
	}

	for (list = *listp; list != NULL; list = list->prev) {
		if (bnl_equals(&list->bni, bnl)) {
			// reference already exists
			return 0;
		}
	}
	list = malloc(sizeof *list);
	list->prev = *listp;
	list->bni.str = bnl->str;
	list->bni.len = bnl->len;
	*listp = list;
	return 0;
}

void specialize_bni(struct blank_node_label *bni, const struct blank_node_label *ref_bni) {
	const char *replace_with = bnl_equals(bni, ref_bni) ? "_:a" : "_:z";
	bni->str = replace_with;
	if (replace_with == NULL) err(1, "NO1");
	bni->len = 3;
}

/*
void bni_set(struct blank_node_label *dest, const struct blank_node_label *src) {
	if (src->str == NULL) err(1, "NO");
	dest->str = src->str;
	dest->len = src->len;
}
*/

ssize_t rdf_subject_length(struct rdf_subject *subj) {
	switch (subj->type) {
		case rdf_term_type_iri_ref:
			return subj->value.iri_ref.len;
		case rdf_term_type_blank_node_label:
			return subj->value.blank_node_label.len;
		default:
			errno = EPROTO;
			return -1;
	}
}

ssize_t rdf_object_length(struct rdf_object *obj) {
	switch (obj->type) {
		case rdf_term_type_iri_ref:
			return obj->value.iri_ref.len;
		case rdf_term_type_blank_node_label:
			return obj->value.blank_node_label.len;
		case rdf_term_type_literal:
			return obj->value.literal.len;
		default:
			errno = EPROTO;
			return -1;
	}
}

ssize_t rdf_graph_label_opt_length(struct rdf_graph_label_opt *opt) {
	switch (opt->type) {
		case rdf_term_type_iri_ref:
			return opt->value.iri_ref.len;
		case rdf_term_type_blank_node_label:
			return opt->value.blank_node_label.len;
		case rdf_term_type_null:
			return 0;
		default:
			errno = EPROTO;
			return -1;
	}
}

ssize_t rdf_statement_length(struct rdf_statement *stmt) {
	size_t len = 0;
	ssize_t l = rdf_subject_length(&stmt->subject);
	if (l < 0) return -1;
	len += l + 1;
	len += stmt->predicate.iri_ref.len + 1;
	l = rdf_object_length(&stmt->object);
	if (l < 0) return -1;
	len += l + 1;
	l = rdf_graph_label_opt_length(&stmt->graph_label_opt);
	if (l < 0) return -1;
	if (l > 0) len += l + 1;
	len += 3; // ".\n\0"
	return len;
}

char *rdf_statement_serialize(struct rdf_statement *stmt) {
	if (!stmt->modified) return strdup(stmt->line);
	ssize_t len = rdf_statement_length(stmt);
	if (len < 0) return NULL;
	char *line = malloc(len), *str = line;
	if (line == NULL) return NULL;

	size_t term_len;
	const char *term_str;

	switch (stmt->subject.type) {
		case rdf_term_type_iri_ref: 
			term_len = stmt->subject.value.iri_ref.len;
			term_str = stmt->subject.value.iri_ref.str;
			break;
		case rdf_term_type_blank_node_label: 
			term_len = stmt->subject.value.blank_node_label.len;
			term_str = stmt->subject.value.blank_node_label.str;
			break;
		default:
			goto bad;
	}
	strncpy(str, term_str, term_len);
	str += term_len;
	*str++ = ' ';

	term_len = stmt->predicate.iri_ref.len;
	term_str = stmt->predicate.iri_ref.str;
	strncpy(str, term_str, term_len);
	str += term_len;
	*str++ = ' ';

	switch (stmt->object.type) {
		case rdf_term_type_iri_ref: 
			term_len = stmt->object.value.iri_ref.len;
			term_str = stmt->object.value.iri_ref.str;
			break;
		case rdf_term_type_blank_node_label: 
			term_len = stmt->object.value.blank_node_label.len;
			term_str = stmt->object.value.blank_node_label.str;
			break;
		case rdf_term_type_literal: 
			term_len = stmt->object.value.literal.len;
			term_str = stmt->object.value.literal.str;
			break;
		default:
			goto bad;
	}
	strncpy(str, term_str, term_len);
	str += term_len;
	*str++ = ' ';

	switch (stmt->graph_label_opt.type) {
		case rdf_term_type_null: 
			term_len = 0;
			term_str = "";
			break;
		case rdf_term_type_iri_ref: 
			term_len = stmt->graph_label_opt.value.iri_ref.len;
			term_str = stmt->graph_label_opt.value.iri_ref.str;
			break;
		case rdf_term_type_blank_node_label: 
			term_len = stmt->graph_label_opt.value.blank_node_label.len;
			term_str = stmt->graph_label_opt.value.blank_node_label.str;
			break;
		default:
			goto bad;
	}
	if (term_len > 0) {
		strncpy(str, term_str, term_len);
		str += term_len;
		*str++ = ' ';
	}

	*str++ = '.';
	*str++ = '\n';
	*str++ = '\0';
	return line;
bad:
	free(line);
	errno = EUCLEAN;
	return NULL;
}

char *serialize_nquad_special(struct rdf_statement *quad, const struct blank_node_label *ref_bni) {
	struct rdf_statement quad_copy;
	memcpy(&quad_copy, quad, sizeof *quad);
	if (quad_copy.subject.type == rdf_term_type_blank_node_label) {
		specialize_bni(&quad_copy.subject.value.blank_node_label, ref_bni);
		quad_copy.modified = true;
	}
	if (quad_copy.object.type == rdf_term_type_blank_node_label) {
		specialize_bni(&quad_copy.object.value.blank_node_label, ref_bni);
		quad_copy.modified = true;
	}
	if (quad_copy.graph_label_opt.type == rdf_term_type_blank_node_label) {
		specialize_bni(&quad_copy.graph_label_opt.value.blank_node_label, ref_bni);
		quad_copy.modified = true;
	}
	return rdf_statement_serialize(&quad_copy);
}

int compare_nquads(const void *a, const void *b) {
	return strcmp(*(const char **)a, *(const char **)b);
}

struct quads_refs *get_quads(struct blank_node_to_quads_map *map, const struct blank_node_label *bni) {
	// look up entry in map for blank node identifier
	for (; map != NULL; map = map->prev) {
		if (bnl_equals(&map->bni, bni)) {
			return map->quads;
		}
	}
	return NULL;
}

// https://json-ld.github.io/rdf-dataset-canonicalization/spec/#hash-first-degree-quads
int hash_first_degree_quads(struct hash *hash, struct canonicalization_state *state,
		const struct blank_node_label *ref_bni) {
	int err;
	struct quads_refs *quads = get_quads(state->blank_node_to_quads_map, ref_bni), *q;
	size_t num_nquads = 0;
	for (q = quads; q != NULL; q = q->prev) {
		num_nquads++;
	}
	char **nquads = calloc(num_nquads, sizeof *nquads);
	if (nquads == NULL) return -1;
	size_t i = 0;
	for (; quads != NULL; quads = quads->prev) {
		struct rdf_statement *quad = quads->quad;
		char *nquad = serialize_nquad_special(quad, ref_bni);
		if (nquad == NULL) goto error;
		nquads[i++] = nquad;
	}
	qsort(nquads, num_nquads, sizeof *nquads, compare_nquads);
	struct sha256 sha256;
	sha256_init(&sha256);
	for (i = 0; i < num_nquads; i++) {
		char *nquad = nquads[i];
		size_t len = strlen(nquad);
		sha256_update(&sha256, nquad, len);
		free(nquad);
	}
	sha256_sum(&sha256, hash->data);
	free(nquads);
	return 0;

error:
	err = errno;
	free(nquads);
	errno = err;
	return -1;
}

const struct blank_node_identifier *get_issued_identifier(struct issued_identifiers_list_item *issued_identifiers_list, const struct blank_node_label *identifier) {
	struct issued_identifiers_list_item *i;
	for (i = issued_identifiers_list; i != NULL; i = i->prev) {
		if (bnl_equals(&i->existing_identifier, identifier)) {
			return &i->issued_identifier;
		}
	}
	return NULL;
}

// https://json-ld.github.io/rdf-dataset-canonicalization/spec/#issue-identifier-algorithm
const struct blank_node_identifier *issue_identifier(struct blank_node_identifier_issuer_state *identifier_issuer, const struct blank_node_label *identifier) {
	const struct blank_node_identifier *issued_identifier = get_issued_identifier(identifier_issuer->issued_identifiers_list, identifier);
	if (issued_identifier != NULL) {
		return issued_identifier;
	}
	char id[128];
	int len = snprintf(id, sizeof id, "%s%u", identifier_issuer->identifier_prefix,
		identifier_issuer->identifier_counter++);
	if (len < 0) return NULL;
	if ((size_t) len >= sizeof id) {
		errno = EMSGSIZE;
		return NULL;
	}
	struct issued_identifiers_list_item *list_item = malloc(sizeof *list_item);
	if (list_item == NULL) return NULL;
	char *str = strdup(id);
	if (str == NULL) {
		int err = errno;
		free(list_item);
		errno = err;
		return NULL;
	}
	list_item->prev = identifier_issuer->issued_identifiers_list;
	list_item->existing_identifier.str = identifier->str;
	list_item->existing_identifier.len = identifier->len;
	list_item->issued_identifier.str = str;
	list_item->issued_identifier.len = len;
	identifier_issuer->issued_identifiers_list = list_item;
	return &list_item->issued_identifier;
}

int compare_htbnmh_by_hash(const void *p1, const void *p2) {
	const struct hash_to_blank_nodes_map *const *htbnm1p = p1, *htbnm1 = *htbnm1p;
	const struct hash_to_blank_nodes_map *const *htbnm2p = p2, *htbnm2 = *htbnm2p;
	return hash_compare(&htbnm1->hash, &htbnm2->hash);
}

char char_hex(unsigned char c) {
	switch (c) {
		case 0: return '0';
		case 1: return '1';
		case 2: return '2';
		case 3: return '3';
		case 4: return '4';
		case 5: return '5';
		case 6: return '6';
		case 7: return '7';
		case 8: return '8';
		case 9: return '9';
		case 0xa: return 'a';
		case 0xb: return 'b';
		case 0xc: return 'c';
		case 0xd: return 'd';
		case 0xe: return 'e';
		case 0xf: return 'f';
		default: return 'x';
	}
}

void hash_hex(char buf[65], const struct hash *hash) {
	size_t i;
	size_t j = 0;
	for (i = 0; i < 32; i++) {
		unsigned char b = hash->data[i];
		buf[j++] = char_hex(b >> 4);
		buf[j++] = char_hex(b & 0xf);
	}
	buf[j] = '\0';
}

// https://json-ld.github.io/rdf-dataset-canonicalization/spec/#hash-related-blank-node
int hash_related_blank_node(struct hash *hashp, struct canonicalization_state *cstate, const struct blank_node_label *related, struct rdf_statement *quad, struct blank_node_identifier_issuer_state *issuer, char position) {
	const struct blank_node_identifier *identifier = get_issued_identifier(cstate->canonical_issuer.issued_identifiers_list, related);
	if (identifier == NULL) {
		identifier = get_issued_identifier(issuer->issued_identifiers_list, related);
		if (identifier == NULL) {
			struct hash hash;
			int rc = hash_first_degree_quads(&hash, cstate, related);
			if (rc < 0) return -1;
			char *str = alloca(65);
			hash_hex(str, &hash);
			struct blank_node_identifier *id = alloca(sizeof *id);
			id->len = 64;
			id->str = str;
			identifier = id;
		}
	}
	struct sha256 sha256;
	sha256_init(&sha256);
	sha256_update(&sha256, &position, 1);
	if (position != 'g') {
		sha256_update(&sha256, quad->predicate.iri_ref.str, quad->predicate.iri_ref.len);
	}
	sha256_update(&sha256, identifier->str, identifier->len);
	sha256_sum(&sha256, hashp->data);
	return 0;
}

int copy_issued_identifiers_list(struct issued_identifiers_list_item **list_copyp, const struct issued_identifiers_list_item *list) {
	struct issued_identifiers_list_item *l, *list_copy, *list_copy_head = NULL;
	size_t len = 0;
	for (l = list; l != NULL; l = l->prev) len++;
	struct issued_identifiers_list_item **array = malloc(len * sizeof *array);
	if (array == NULL) err(1, "malloc issued_identifiers_list_item");
	size_t i = len;
	for (l = list; l != NULL; l = l->prev) {
		array[--i] = l;
	}
	for (i = 0; i < len; i++) {
		list = array[i];
		list_copy = malloc(sizeof *list_copy);
		if (list_copy == NULL) {
			issued_identifiers_list_free(list_copy_head);
			free(array);
			return -1;
		}
		size_t issued_identifier_len = list->issued_identifier.len;
		char *issued_identifier = malloc(issued_identifier_len);
		if (issued_identifier == NULL) {
			issued_identifiers_list_free(list_copy_head);
			free(list_copy);
			free(array);
			return -1;
		}
		memcpy(issued_identifier, list->issued_identifier.str, issued_identifier_len);
		list_copy->existing_identifier.str = list->existing_identifier.str;
		list_copy->existing_identifier.len = list->existing_identifier.len;
		list_copy->issued_identifier.str = issued_identifier;
		list_copy->issued_identifier.len = issued_identifier_len;
		list_copy->prev = list_copy_head;
		list_copy_head = list_copy;
	}
	free(array);
	*list_copyp = list_copy_head;
	return 0;
}

int copy_issuer(struct blank_node_identifier_issuer_state **issuer_copyp, const struct blank_node_identifier_issuer_state *issuer) {
	struct blank_node_identifier_issuer_state *issuer_copy = malloc(sizeof *issuer_copy);
	if (issuer_copy == NULL) return -1;
	strncpy(issuer_copy->identifier_prefix, issuer->identifier_prefix, sizeof issuer->identifier_prefix);
	issuer_copy->identifier_counter = issuer->identifier_counter;
	int rc = copy_issued_identifiers_list(&issuer_copy->issued_identifiers_list, issuer->issued_identifiers_list);
	if (rc < 0) {
		free(issuer_copy);
		return -1;
	}
	*issuer_copyp = issuer_copy;
	return 0;
}

int append(char **strp, size_t *lenp, size_t *capp, const char *append_str, size_t append_len) {
	char *str = *strp;
	size_t len = *lenp;
	size_t cap = *capp;
	if (len + append_len + 1 > cap) {
		cap *= 2;
		str = realloc(str, cap * sizeof *str);
		if (str == NULL) return -1;
		*capp = cap;
		*strp = str;
	}
	memcpy(str + len, append_str, append_len);
	len += append_len;
	str[len] = '\0';
	*lenp = len;
	return 0;
}

int bnl_compare(const struct blank_node_label *a, const struct blank_node_label *b) {
	size_t a_len = a->len, b_len = b->len;
	size_t min_len = a_len < b_len ? a_len : b_len;
	int c = strncmp(a->str, b->str, min_len);
	if (c != 0) return c;
	return a_len < b_len ? -1 : a_len > b_len ? 1 : 0;
}

int compare_bnlp(const void *p1, const void *p2) {
	const struct blank_node_label *const *bnl1p = p1, *bnl1 = *bnl1p;
	const struct blank_node_label *const *bnl2p = p2, *bnl2 = *bnl2p;
	return bnl_compare(bnl1, bnl2);
}

void swap(void **array, int i, int j) {
	void *tmp = array[i];
	array[i] = array[j];
	array[j] = tmp;
}

void swap_range(void **array, int i, int j) {
	while (i < j) {
		swap(array, i, j);
		i++;
		j--;
	}
}

// https://en.wikipedia.org/wiki/Permutation#Generation_in_lexicographic_order
bool permute_bnla(struct blank_node_label **bnla, size_t bnla_len) {
	ssize_t k;
	for (k = bnla_len-2; k >= 0; k--) {
		if (bnl_compare(bnla[k], bnla[k+1]) < 0) break;
	}
	if (k < 0) return false;
	size_t j;
	for (j = bnla_len-1; j > (size_t)k; j--) {
		if (bnl_compare(bnla[k], bnla[j]) < 0) break;
	}
	swap((void **)bnla, k, j);
	swap_range((void **)bnla, k+1, bnla_len-1);
	return true;
}

// https://json-ld.github.io/rdf-dataset-canonicalization/spec/#hash-n-degree-quads
int hash_n_degree_quads(struct hash_path_list_item *result, struct canonicalization_state *cstate, struct blank_node_identifier_issuer_state *issuer, const struct blank_node_label *identifier) {
	struct issued_identifiers_list_item *id;
	struct hash_to_blank_nodes_map *hash_to_related_blank_nodes_map = NULL;
	struct quads_refs *quads = get_quads(cstate->blank_node_to_quads_map, identifier), *q;
	int rc;
	struct blank_node_label *bni;
	rc = copy_issuer(&issuer, issuer);
	if (rc < 0) return -1;
	struct hash hash;
	for (q = quads; q != NULL; q = q->prev) {
		struct rdf_statement *quad = q->quad;
		if (quad == NULL) { errno = EUCLEAN; return -1; }
		if (quad->subject.type == rdf_term_type_blank_node_label) {
			bni = &quad->subject.value.blank_node_label;
			if (!bnl_equals(bni, identifier)) {
				rc = hash_related_blank_node(&hash, cstate, bni, quad, issuer, 's');
				if (rc < 0) return -1;
				rc = add_hash_id(&hash_to_related_blank_nodes_map, hash, bni);
				if (rc < 0) return -1;
			}
		}
		if (quad->object.type == rdf_term_type_blank_node_label) {
			bni = &quad->object.value.blank_node_label;
			if (!bnl_equals(bni, identifier)) {
				rc = hash_related_blank_node(&hash, cstate, bni, quad, issuer, 'o');
				if (rc < 0) return -1;
				rc = add_hash_id(&hash_to_related_blank_nodes_map, hash, bni);
				if (rc < 0) return -1;
			}
		}
		if (quad->graph_label_opt.type == rdf_term_type_blank_node_label) {
			bni = &quad->graph_label_opt.value.blank_node_label;
			if (!bnl_equals(bni, identifier)) {
				rc = hash_related_blank_node(&hash, cstate, bni, quad, issuer, 'g');
				if (rc < 0) return -1;
				rc = add_hash_id(&hash_to_related_blank_nodes_map, hash, bni);
				if (rc < 0) return -1;
			}
		}
	}
	struct sha256 sha256;
	sha256_init(&sha256);

	size_t num_hashes = 0;
	struct hash_to_blank_nodes_map *map = hash_to_related_blank_nodes_map, *m;
	for (m = map; m != NULL; m = m->prev) num_hashes++;
	struct hash_to_blank_nodes_map **htbnma = malloc(num_hashes * sizeof *htbnma);
	if (htbnma == NULL) return -1;
	size_t i = 0;
	for (m = map; m != NULL; m = m->prev) htbnma[i++] = m;
	qsort(htbnma, num_hashes, sizeof *htbnma, compare_htbnmh_by_hash);
	for (i = 0; i < num_hashes; i++) {
		struct hash_to_blank_nodes_map *htbnm = htbnma[i];
		struct hash *related_hash = &htbnm->hash;
		struct blank_node_identifier_list *blank_node_list = htbnm->list, *l;
		char related_hash_hex[65];
		hash_hex(related_hash_hex, related_hash);
		sha256_update(&sha256, related_hash_hex, 64);
		char *chosen_path = NULL;
		size_t chosen_path_len = 0;
		struct blank_node_identifier_issuer_state *chosen_issuer = NULL;
		size_t bnla_len = 0;
		for (l = blank_node_list; l != NULL; l = l->prev) bnla_len++;
		struct blank_node_label **bnla = malloc((bnla_len + 1) * sizeof *bnla);
		if (bnla == NULL) goto bad;
		size_t j = 0;
		for (l = blank_node_list; l != NULL; l = l->prev) bnla[j++] = &l->bni;
		bnla[j] = NULL;
		// Permute through bnla
		do {
			struct blank_node_label **permutation = bnla;
			struct blank_node_identifier_issuer_state *issuer_copy;
			rc = copy_issuer(&issuer_copy, issuer);
			if (rc < 0) goto bad;
			size_t cap = 128;
			char *path = malloc(cap * sizeof *path);
			if (path == NULL) goto bad;
			path[0] = '\0';
			size_t path_len = 0;
			struct blank_node_identifier_list *recursion_list = NULL;
			size_t recursion_list_len = 0;
			struct blank_node_label *related;
			for (j = 0; permutation[j] != NULL; j++) {
				related = permutation[j];
				const struct blank_node_identifier *canonical_identifier = get_issued_identifier(cstate->canonical_issuer.issued_identifiers_list, related);
				if (canonical_identifier != NULL) {
					rc = append(&path, &path_len, &cap, canonical_identifier->str, canonical_identifier->len);
					if (rc < 0) goto bad;
				} else {
					const struct blank_node_identifier *identifier = get_issued_identifier(issuer_copy->issued_identifiers_list, related);
					if (identifier == NULL) {
						struct blank_node_identifier_list *list = malloc(sizeof *list);
						if (list == NULL) goto bad;
						list->bni.str = related->str;
						list->bni.len = related->len;
						list->prev = recursion_list;
						recursion_list = list;
						recursion_list_len++;
					}
					const struct blank_node_identifier *issued_identifier = issue_identifier(issuer_copy, related);
					if (rc < 0) goto bad;
					rc = append(&path, &path_len, &cap, issued_identifier->str, issued_identifier->len);
					if (rc < 0) goto bad;
				}
				if (chosen_path != NULL && path_len >= chosen_path_len && strcmp(path, chosen_path) > 0) goto next_permutation;
			}
			// Reverse linked list to iterate in original order
			size_t k = recursion_list_len;
			struct blank_node_identifier_list **recursion_array = malloc(recursion_list_len * sizeof *recursion_list);
			if (recursion_array == NULL) err(1, "malloc recursion list (%zd)", recursion_list_len);
			for (l = recursion_list; l != NULL; l = l->prev) {
				recursion_array[--k] = l;
			}
			for (k = 0; k < recursion_list_len; k++) {
				related = &recursion_array[k]->bni;
				struct hash_path_list_item result_r;
				rc = hash_n_degree_quads(&result_r, cstate, issuer_copy, related);
				const struct blank_node_identifier *issued_identifier = issue_identifier(issuer_copy, related);
				if (rc < 0) goto bad;
				rc = append(&path, &path_len, &cap, issued_identifier->str, issued_identifier->len);
				if (rc < 0) goto bad;
				char str[66] = "<";
				hash_hex(str + 1, &result_r.hash);
				str[65] = '>';
				rc = append(&path, &path_len, &cap, str, 66);
				if (rc < 0) goto bad;
				issuer_state_free(issuer_copy);
				issuer_copy = result_r.issuer;
				if (chosen_path != NULL && path_len >= chosen_path_len && strcmp(path, chosen_path) > 0) goto next_permutation;
			}
			free(recursion_array);
			if (chosen_path == NULL || (strcmp(path, chosen_path) < 0)) {
				if (chosen_path != NULL) free(chosen_path);
				if (chosen_issuer != NULL) issuer_state_free(chosen_issuer);
				chosen_path = path;
				chosen_path_len = path_len;
				chosen_issuer = issuer_copy;
			} else {
				free(path);
			}
next_permutation:;
		} while (permute_bnla(bnla, bnla_len));
		free(bnla);
		sha256_update(&sha256, chosen_path, chosen_path_len);
		free(chosen_path);
		issuer_state_free(issuer);
		issuer = chosen_issuer;
	}
	for (i = 0; i < num_hashes; i++) {
		// TODO make this work since existing identifiers still referenced?
		struct hash_to_blank_nodes_map *htbnm = htbnma[i];
		free(htbnm);
		// hash_to_blank_nodes_map_free(htbnm);
	}
	free(htbnma);
	// TODO: free issuer copies?

	result->issuer = issuer;
	sha256_sum(&sha256, result->hash.data);
	return 0;

bad:
	// TODO: free paths and issuer copies
	free(htbnma);
	return -1;
}

int compare_hash_path_list_itemp(const void *p1, const void *p2) {
	const struct hash_path_list_item *const *item1p = p1, *item1 = *item1p;
	const struct hash_path_list_item *const *item2p = p2, *item2 = *item2p;
	return hash_compare(&item1->hash, &item2->hash);
}

char *serialize_canonicalized(struct rdf_statement *quad, struct blank_node_identifier_issuer_state *issuer) {
	struct rdf_statement quad_copy;
	memcpy(&quad_copy, quad, sizeof *quad);
	const struct blank_node_identifier *issued_identifier;
	struct blank_node_label *bni;
	struct issued_identifiers_list_item *issued_identifiers_list = issuer->issued_identifiers_list;
	if (quad_copy.subject.type == rdf_term_type_blank_node_label) {
		bni = &quad_copy.subject.value.blank_node_label;
		issued_identifier = get_issued_identifier(issued_identifiers_list, bni);
		if (issued_identifier == NULL) errx(1, "expected issued identifier for %.*s", (int)bni->len, bni->str);
		bni->str = issued_identifier->str;
		bni->len = issued_identifier->len;
		quad_copy.modified = true;
	}
	if (quad_copy.object.type == rdf_term_type_blank_node_label) {
		bni = &quad_copy.object.value.blank_node_label;
		issued_identifier = get_issued_identifier(issued_identifiers_list, bni);
		if (issued_identifier == NULL) errx(1, "expected issued identifier for %s", bni->str);
		bni->str = issued_identifier->str;
		bni->len = issued_identifier->len;
		quad_copy.modified = true;
	}
	if (quad_copy.graph_label_opt.type == rdf_term_type_blank_node_label) {
		bni = &quad_copy.graph_label_opt.value.blank_node_label;
		issued_identifier = get_issued_identifier(issued_identifiers_list, bni);
		if (issued_identifier == NULL) errx(1, "expected issued identifier");
		bni->str = issued_identifier->str;
		bni->len = issued_identifier->len;
		quad_copy.modified = true;
	}
	// Note: not returning rdf_statement struct, because term string pointers would need to be updated.
	return rdf_statement_serialize(&quad_copy);
}

int main() {
	allocs = 0;
	frees = 0;
	int rc;
	struct rdf_statement_list *stmts = NULL;
	size_t num_stmts = 0;
	while ((rc = readnquad(&stmts)) == 0) num_stmts++;
	if (rc < 0) err(1, "read nquad");

	// https://json-ld.github.io/rdf-dataset-canonicalization/spec/#algorithm
	// Create the canonicalization state.
	struct canonicalization_state cstate;
	canonicalization_state_init(&cstate);
	struct blank_node_identifier_issuer_state *cissuer = &cstate.canonical_issuer;
	struct issued_identifiers_list_item *id;

	struct rdf_statement_list *s;
	for (s = stmts; s != NULL; s = s->prev) {
		struct rdf_statement *quad = &s->stmt;
		if (quad->subject.type == rdf_term_type_blank_node_label) {
			rc = add_quad_ref(&cstate.blank_node_to_quads_map, &quad->subject.value.blank_node_label, quad);
			if (rc < 0) err(1, "add_quad_ref");
		}
		if (quad->object.type == rdf_term_type_blank_node_label) {
			rc = add_quad_ref(&cstate.blank_node_to_quads_map, &quad->object.value.blank_node_label, quad);
			if (rc < 0) err(1, "add_quad_ref");
		}
		if (quad->graph_label_opt.type == rdf_term_type_blank_node_label) {
			rc = add_quad_ref(&cstate.blank_node_to_quads_map, &quad->graph_label_opt.value.blank_node_label, quad);
			if (rc < 0) err(1, "add_quad_ref");
		}
	}

	struct blank_node_identifier_list *non_normalized_identifiers = NULL;
	struct blank_node_to_quads_map *map;
	for (map = cstate.blank_node_to_quads_map; map != NULL; map = map->prev) {
		struct blank_node_identifier_list *list = malloc(sizeof *list);
		if (list == NULL) err(1, "malloc blank node identifier list");
		list->bni.str = map->bni.str;
		list->bni.len = map->bni.len;
		list->prev = non_normalized_identifiers;
		non_normalized_identifiers = list;
	}

	struct hash_to_blank_nodes_map **htbnma;

	bool simple = true;
	while (simple) {
		simple = false;
		hash_to_blank_nodes_map_free(cstate.hash_to_blank_nodes_map);
		cstate.hash_to_blank_nodes_map = NULL;
		struct blank_node_identifier_list *list;
		for (list = non_normalized_identifiers; list != NULL; list = list->prev) {
			struct hash hash;
			rc = hash_first_degree_quads(&hash, &cstate, &list->bni);
/*
			warnx("bni [%.*s] hash %02x%02x%02x", (int)list->bni.len, list->bni.str,
				hash.data[0], hash.data[1], hash.data[2]);
*/
			if (rc < 0) err(1, "hash_first_degree_quads");
			rc = add_hash_id(&cstate.hash_to_blank_nodes_map, hash, &list->bni);
			if (rc < 0) err(1, "add_hash_id");
		}

		// Get hashes from hash_to_blank_nodes_map, sorted.
		size_t num_hashes = 0;
		struct hash_to_blank_nodes_map *map = cstate.hash_to_blank_nodes_map, *m;
		// TODO: store the length already computed
		for (m = map; m != NULL; m = m->prev) num_hashes++;
		htbnma = malloc(num_hashes * sizeof *htbnma);
		if (htbnma == NULL) err(1, "malloc htbnma");
		size_t i = 0;
		for (m = map; m != NULL; m = m->prev) htbnma[i++] = m;
		qsort(htbnma, num_hashes, sizeof *htbnma, compare_htbnmh_by_hash);
		for (i = 0; i < num_hashes; i++) {
			struct hash_to_blank_nodes_map *htbnm = htbnma[i];
			struct blank_node_identifier_list *list = htbnm->list, *l;
			size_t list_len = 0;
			for (l = list; l != NULL; l = l->prev) list_len++;
			if (list_len > 1) continue;
			struct blank_node_label *identifier = &list->bni;
			const struct blank_node_identifier *issued_identifier = issue_identifier(&cstate.canonical_issuer, identifier);
			if (issued_identifier == NULL) err(1, "issue_identifier");
			blank_node_identifier_list_remove(&non_normalized_identifiers, identifier);
			// Don't need to free/remove htbnm, since it gets cleared
			// in the next iteration of the outer loop, and is not used before then.
			simple = true;
		}
		free(htbnma);
	}

	struct blank_node_identifier_list *bnil, *prev_bnil;
	for (bnil = non_normalized_identifiers; bnil != NULL; bnil = prev_bnil) {
		prev_bnil = bnil->prev;
		free(bnil);
	}

	size_t num_hashes = 0;
	struct hash_to_blank_nodes_map *htbnm = cstate.hash_to_blank_nodes_map, *m;
	// TODO: store the length already computed
	for (m = htbnm; m != NULL; m = m->prev) num_hashes++;
	htbnma = malloc(num_hashes * sizeof *htbnma);
	if (htbnma == NULL) err(1, "malloc htbnma");
	size_t i = 0;
	for (m = htbnm, i = 0; m != NULL; m = m->prev) htbnma[i++] = m;
	qsort(htbnma, num_hashes, sizeof *htbnma, compare_htbnmh_by_hash);

	for (i = 0; i < num_hashes; i++) {
		htbnm = htbnma[i];
		struct blank_node_identifier_list *list = htbnm->list, *l;
		size_t num_identifiers = 0;
		for (l = list; l != NULL; l = l->prev) {
			num_identifiers++;
		}
		struct hash_path_list_item *hash_path_list = calloc(num_identifiers, sizeof *hash_path_list);
		if (hash_path_list == NULL) err(1, "calloc hash_path_list");
		// Use separate array of pointers so less copying is needed during sorting.
		struct hash_path_list_item **hash_path_list_array = calloc(num_identifiers, sizeof *hash_path_list_array);
		if (hash_path_list_array == NULL) err(1, "calloc hash_path_list_array");
		size_t j = 0;
		for (l = list; l != NULL; l = l->prev) {
			struct blank_node_label *identifier = &l->bni;
			bool has_canonical_identifier_issued = get_issued_identifier(cstate.canonical_issuer.issued_identifiers_list, identifier) != NULL;
			if (has_canonical_identifier_issued) continue;
			struct blank_node_identifier_issuer_state temporary_issuer;
			identifier_issuer_init(&temporary_issuer, "_:b");
			const struct blank_node_identifier *issued_identifier = issue_identifier(&temporary_issuer, identifier);
			if (issued_identifier == NULL) err(1, "issue temporary identifier");
			struct hash_path_list_item *result = &hash_path_list[j];
			rc = hash_n_degree_quads(result, &cstate, &temporary_issuer, identifier);
			if (rc < 0) err(1, "hash n-degree quads");
			issuer_state_deinit(&temporary_issuer);
			hash_path_list_array[j++] = result;
		}
		const size_t hash_path_list_array_len = j;
		qsort(hash_path_list_array, hash_path_list_array_len, sizeof *hash_path_list_array, compare_hash_path_list_itemp);
		for (j = 0; j < hash_path_list_array_len; j++) {
			struct hash_path_list_item *result = hash_path_list_array[j];
			struct blank_node_identifier_issuer_state *identifier_issuer = result->issuer;
			struct issued_identifiers_list_item *item;
			size_t num_items = 0;
			for (item = identifier_issuer->issued_identifiers_list; item != NULL; item = item->prev) {
				num_items++;
			}
			struct blank_node_label **existing_identifiers = malloc(num_items * sizeof *existing_identifiers);
			if (existing_identifiers == NULL) err(1, "malloc existing identifiers (%zd)", num_items);
			size_t k = num_items;
			// Reverse linked list to visit in same order.
			struct blank_node_label *existing_identifier;
			for (item = identifier_issuer->issued_identifiers_list; item != NULL; item = item->prev) {
				existing_identifier = &item->existing_identifier;
				existing_identifiers[--k] = existing_identifier;
			}
			for (k = 0; k < num_items; k++) {
				existing_identifier = existing_identifiers[k];
				const struct blank_node_identifier *issued_identifier = issue_identifier(&cstate.canonical_issuer, existing_identifier);
				if (issued_identifier == NULL) err(1, "issue canonical identifier");
			}
			free(existing_identifiers);
			issuer_state_free(result->issuer);
		}
		free(hash_path_list);
		free(hash_path_list_array);
	}
	free(htbnma);

	char **canonicalized_quads = malloc(num_stmts * sizeof *canonicalized_quads);
	if (canonicalized_quads == NULL) err(1, "malloc canonicalized_quads");
	i = 0;
	struct rdf_statement_list *prev;
	for (s = stmts; s != NULL; s = s->prev) {
		struct rdf_statement *quad = &s->stmt;
		char *quad_copy = serialize_canonicalized(quad, &cstate.canonical_issuer);
		if (quad_copy == NULL) err(1, "replace blank node identifiers");
		canonicalized_quads[i++] = quad_copy;
	}
	for (s = stmts; s != NULL; s = prev) {
		prev = s->prev;
		free(s->stmt.line);
		free(s);
	}
	qsort(canonicalized_quads, num_stmts, sizeof *canonicalized_quads, compare_quadsp);
	for (i = 0; i < num_stmts; i++) {
		char *line = canonicalized_quads[i];
		fputs(line, stdout);
		free(line);
	}
	free(canonicalized_quads);
	canonicalization_state_deinit(&cstate);
	if (allocs != frees) warnx("allocs: %d, frees: %d", allocs, frees);
	return 0;
}
