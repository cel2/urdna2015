#!/bin/sh

passed=0
total=0
for input in rdf-dataset-canonicalization/tests/test*-in.nq
do
	echo $input
	output=${input%-in.nq}-urdna2015.nq
	if ./urdna2015 < "$input" > "$output".attempt && cmp "$output" "$output".attempt
	then
		: $((passed += 1))
	fi
	: $((total += 1))
done
echo $passed/$total passed
[ $passed -eq $total ]
