# SPDX-FileCopyrightText: 2022 Charles E. Lehner
# SPDX-License-Identifier: AGPL-3.0-or-later

BIN=urdna2015
MAN=urdna2015.1

PREFIX = /usr/local
BINDIR = $(PREFIX)/bin
MANDIR = $(PREFIX)/share/man
MAN1DIR = $(MANDIR)/man1

CFLAGS=-Wall -Wextra
LDLIBS=-I.

test: $(BIN) rdf-dataset-canonicalization
	./test.sh

all: $(BIN)

$(BIN): $(BIN).c libutil/sha256.o

check:
	mandoc -T lint $(MAN)

clean:
	rm -f $(BIN)

install: all
	mkdir -p "$(DESTDIR)$(BINDIR) "$(DESTDIR)$(MAN1DIR)""
	cp $(BIN) "$(DESTDIR)$(BINDIR)/"
	cp $(MAN) "$(DESTDIR)$(MAN1DIR)/"

uninstall:
	rm -f "$(DESTDIR)$(BINDIR)/$(BIN)" "$(DESTDIR)$(MAN1DIR)/$(MAN)"

link: all
	mkdir -p "$(DESTDIR)$(BINDIR)" "$(DESTDIR)$(MAN1DIR)"
	ln -sf $(shell realpath $(BIN)) "$(DESTDIR)$(BINDIR)/"
	ln -sf $(shell realpath $(MAN)) "$(DESTDIR)$(MAN1DIR)/"

rdf-dataset-canonicalization:
	git clone https://github.com/w3c-ccg/rdf-dataset-canonicalization
